import os
import uvicorn
from database import create_db_and_tables
from fastapi import FastAPI, Depends
from sqlmodel import Session
from models.user_model import Users, Credentials
from controllers.user_controller import get_session, add_user

PRODUCTION = os.environ.get('PRODUCTION')  # Boolean value.


class Bank_API_Application:

    def __init__(self):
        self.app = FastAPI()

        @self.app.on_event('startup')
        def on_startup():
            create_db_and_tables()

        @self.app.get('/')
        async def root():
            return {'message': 'Hello, world!'}

        @self.app.post('/users/')
        async def register(user: Users, credentials: Credentials, session: Session = Depends(get_session)):  # noqa E501
            add_user(session, user=user, credentials=credentials)


app = Bank_API_Application()

if __name__ == '__main__':
    if not PRODUCTION.lower() == 'true' if PRODUCTION else True:
        uvicorn.run(
            '__main__:app.app', reload=True
        )
    else:
        print('No production configuration yet.')
