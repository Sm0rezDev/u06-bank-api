import os
from os.path import join
from dotenv import load_dotenv
from sqlmodel import SQLModel, create_engine
import logging

logging.basicConfig(
    level=logging.INFO,
    format='%(levelname)s:     %(message)s')

dotenv_path = join('conf.env')
load_dotenv(dotenv_path)

MYSQL_URL = os.environ.get('DATABASE_URL')
logging.info(MYSQL_URL)
engine = create_engine(MYSQL_URL, echo=True)


def create_db_and_tables():
    """
    Creates the tables into the database.
    """
    try:
        SQLModel.metadata.create_all(engine)
    except:
        logging.debug('Database does not exist.')
