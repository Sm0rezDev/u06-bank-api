sqlmodel
uvicorn[standard]
fastapi
mysqlclient
pydantic[email]
pylint
httpx
pytest