# Python
FROM python:3.11.3-alpine

WORKDIR /api

COPY api .
COPY requirements.txt .

RUN pip install -r requirements.txt

CMD ["python", "api/app.py"]