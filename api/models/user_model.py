"""
SQLmodel with pydantic validator
"""

import enum
import re
from typing import Optional
from sqlalchemy import Enum, Column
from sqlmodel import Field, SQLModel
from pydantic import validator


class Users(SQLModel, table=True):
    """
    Users table.
    """
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str = Field(max_length=50)
    email: str = Field(max_length=50)
    address: str = Field(max_length=50)

    @validator('name')
    def name_must_contain_space(name):
        """
        Validates if the name contain a space.
        """
        if ' ' not in name:
            raise ValueError('must contain a space')
        return name.title()

    @validator('email')
    def valid_domain(email):
        """
        Validates the email for a matching domain name using regular
        expression matching.
        """
        pattern = r'^[A-Za-z0-9]+@(gmail.com|outlook.com|protonmail.com|yahoo.com|icloud.com)$'
        if not re.match(pattern, email):
            raise ValueError('Invalid email domain')
        return email


class Credentials(SQLModel, table=True):
    """
    User credential table.
    """
    id: Optional[int] = Field(default=None, primary_key=True)
    user_id: Optional[int] = Field(default=None, foreign_key='users.id')
    username: str = Field(max_length=50)
    password: str = Field(max_length=32)

    # Needs password validator and maybe username(for explict usernames)


class Account(SQLModel, table=True):
    """
    Customer account table.
    """
    id: Optional[int] = Field(default=None, primary_key=True)
    user_id: Optional[int] = Field(default=None, foreign_key='users.id')
    balance: float = Field(default=0)
    account_number: int


class TransactionEnumDirection(enum.Enum):
    """
    Enums for table Transactions
    """
    TX = 'Send'
    RX = 'Receive'


class Transactions(SQLModel, table=True):
    """
    Transaction table.
    Table contents showing how much money was transferred
    from origin to the receiver.
    """
    id: Optional[int] = Field(default=None, primary_key=True)
    direction: TransactionEnumDirection = Column(
        Enum(TransactionEnumDirection)
    )
    amount: float = Field(default=0)
    origin: int = Field(default=None, foreign_key='users.id')
